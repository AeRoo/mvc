<?php

require_once "./view/abstractView.php";

class RegisterView extends AbstractView {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function render() {
        $error = $this->createError($this->model->getError());
        echo "
        <head>
            <link rel='stylesheet' href='/static/global.css'>
        </head>
        <body>
            <a href='/login'>login instead</a>
            <br><br>
            <div class='center'>
                $error
                <form method='post' action='/register'>
                    <h2>Register</h2>
                    <input name='login' placeholder='user'>
                    <br><br>
                    <input type='password' name='password' placeholder='password'>
                    <br><br>
                    <input type='submit'>
                </form>
            </div>
        </body>
        ";
    }
}