<?php

require_once "./view/abstractView.php";

class EditView extends AbstractView {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function render() {
        $error = $this->createError($this->model->getError());
        echo "
        <head>
            <link rel='stylesheet' href='/static/global.css'>
            <link rel='stylesheet' href='/static/edit.css'>
        </head>
        <body>
            <div class='nav'>
                <a href='/'><h2>Cancel</h2></a>
                <a href='/logout'><h2>Log out ({$_SESSION["login"]})</h2></a>
            </div>
            <div class='center'>
                $error
                <h3>Edit task: {$this->model->task_id}</h3>
                <form method='post' action='/delete?id={$this->model->task_id}'>
                    <input type='submit' value='delete'>
                </form>
                <form method='post' action='/edit?id={$this->model->task_id}'>
                    <input name='title' value='{$this->model->title}'>
                    <br><br>
                    <textarea name='content'>{$this->model->content}</textarea>
                    <br><br>
                    <input type='submit'>
                </form>
            </div>
        </body>
        ";
    }
}