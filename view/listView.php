<?php

require_once "./view/abstractView.php";

class ListView extends AbstractView {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    private function getTasks() {
        $tasks = "";
        $result = $this->model->getTasks();
        foreach ($result as $task) {
            $tasks .= "<a class='task' href='/edit?id=$task[0]'><div>";
            $tasks .= "<h1>$task[1]</h1>";
            $tasks .= "<p>" . $task[2] . "</p>";
            $tasks .= "</div></a>";
        }
        return $tasks;
    }

    public function render() {
        $tasks = $this->getTasks();
        echo "
        <head>
            <link rel='stylesheet' href='/static/global.css'>
            <link rel='stylesheet' href='/static/list.css'>
        </head>
        <body>
            <a href='/logout' class='logout'><h2>Log out ({$_SESSION["login"]})</h2></a>
            <div class='wrapper'>
                <div class='center'>
                    <h3>Add task: </h3>
                    <form method='post' action='/add'>
                        <input name='title' placeholder='Task title'>
                        <br><br>
                        <textarea name='content' placeholder='content'></textarea>
                        <br><br>
                        <input type='submit'>
                    </form>
                </divi>
                <div class='grid'>
                    $tasks
                </div>
            </div>
        </body>
        ";
    }
}