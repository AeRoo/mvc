<?php

class AbstractView {
    public function __construct($model) {
    }

    function createError($text) {
        if (!empty($text)) {
            return "
            <div class='error'>
                <h2>$text</h2>
            </div>
            ";
        }
        return "";
    }

    public function render() {
    }
}
