<?php

require_once "./app/database.php";

class UserModel {
    private $db;
    private $error;

    public function __construct() {
        $this->db = new Database();
    }

    public function showError($text) {
        $this->error = $text;
    }

    public function getError() {
        return $this->error;
    }

    public function login($username, $password) {
        $stmt = $this->db->get()->prepare("SELECT user_id, hash FROM users WHERE name = (?)");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $result = $stmt->get_result()->fetch_row();
        $id = $result[0];
        $hash = $result[1];
        if (password_verify($password, $hash)) {
            $_SESSION["auth"] = true;
            $_SESSION["id"] = $id;
            $_SESSION["login"] = $username;
            return true;
        }
        return false;
    }

    public function register($username, $password) {
        $hash = password_hash($password, PASSWORD_ARGON2I);
        try {
            $stmt = $this->db->get()->prepare("INSERT INTO users (name, hash) VALUES (?, ?)");
            $stmt->bind_param("ss", $username, $hash);
            if (!$stmt->execute()) {
                return false;
            }
            $_SESSION["auth"] = true;
            $_SESSION["id"] = $stmt->insert_id;
            $_SESSION["login"] = $username;
            return true;
        } catch (mysqli_sql_exception $e) {
            return false;
        }
    }
}