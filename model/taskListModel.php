<?php

require_once "./app/database.php";

if (!isset($_SESSION["auth"]) || $_SESSION["auth"] !== true) {
    header("location: ../login");
    die();
}

class TaskListModel {
    private $db;

    public function __construct() {
        $this->db = new Database();
    }

    public function getTasks() {
        $mysqli = $this->db->get();
        $stmt = $mysqli->prepare("SELECT task_id, title, content FROM tasks WHERE user_id = (?)");
        $stmt->bind_param("i", $_SESSION["id"]);
        $stmt->execute();
        return mysqli_fetch_all($stmt->get_result());
    }

    public function addTask($title, $content) {
        $mysqli = $this->db->get();
        $stmt = $mysqli->prepare("INSERT INTO tasks (user_id, title, content) VALUES (?, ?, ?)");
        $stmt->bind_param("iss", $_SESSION["id"], $title, $content);
        $stmt->execute();
    }
}