<?php

require_once "./app/database.php";

if (!isset($_SESSION["auth"]) || $_SESSION["auth"] !== true) {
    header("location: ../login");
    die();
}

class TaskModel {
    private $db;
    private $error;

    public $task_id;
    public $title;
    public $content;

    public function __construct() {
        $this->db = new Database();
    }

    public function showError($text) {
        $this->error = $text;
    }

    public function getError() {
        return $this->error;
    }

    public function setTaskId($id) {
        $this->task_id = $id;
    }

    public function update() {
        $data = $this->loadData();
        $result = $data->fetch_row();
        if ($result == null) {
            return false;
        }
        $this->title = $result[0];
        $this->content = $result[1];
        return true;
    }

    private function loadData() {
        $stmt = $this->db->get()->prepare("SELECT title, content FROM tasks WHERE task_id = (?)");
        $stmt->bind_param("i", $this->task_id);
        $stmt->execute();
        return $stmt->get_result();
    }

    public function updateTask($title, $content) {
        $stmt = $this->db->get()->prepare("UPDATE tasks SET title = (?), content = (?) WHERE task_id = (?)");
        $stmt->bind_param("ssi", $title, $content, $this->task_id);
        $stmt->execute();
    }

    public function deleteTask() {
        $stmt = $this->db->get()->prepare("DELETE FROM tasks WHERE task_id = (?)");
        $stmt->bind_param("i", $this->task_id);
        $stmt->execute();
    }
}