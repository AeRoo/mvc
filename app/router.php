<?php

class Route {
    private $model;
    private $view;
    private $controller;

    public function __construct($model, $view, $controller) {
        $this->model = $model;
        $this->view = $view;
        $this->controller = $controller;
    }

    public function handle() {
        $root = __DIR__ . "/..";
        require_once $root . "/model/" . $this->model . ".php";
        require_once $root . "/view/" . $this->view . ".php";
        require_once $root . "/controller/" . $this->controller . ".php";

        $model = new $this->model;
        $view = new $this->view($model);
        $controller = new $this->controller($model);
        $controller->handle();
        $view->render();
    }
}

class Router {
    private $options = [];

    public function handle($path, $route) {
        $this->options[$path] = $route;
    }

    private function check($uri) {
        if (!isset($this->options[$uri])) {
            http_response_code(404);
            echo "not found<br>";
            die();
        }
    }

    public function serve() {
        $uri = strtok($_SERVER["REQUEST_URI"], "?");
        $this->check($uri);

        $route = $this->options[$uri];
        $route->handle();
    }
}