<?php

require_once "./app/config.php";

class Database {
    private $conn;

    public function __construct() {
        $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    }

    public function get() {
        return $this->conn;
    }
}