<?php
ob_start();
session_start();

require_once "./app/router.php";
require_once "./app/config.php";

$router = new Router();

$router->handle("/login", new Route("userModel", "loginView", "loginController"));
$router->handle("/register", new Route("userModel", "registerView", "registerController"));
$router->handle("/logout", new Route("userModel", "abstractView", "logoutController"));

$router->handle("/", new Route("taskListModel", "listView", "listController"));
$router->handle("/add", new Route("taskListModel", "abstractView", "addTaskController"));
$router->handle("/edit", new Route("taskModel", "editView", "editTaskController"));
$router->handle("/delete", new Route("taskModel", "abstractView", "deleteTaskController"));

$router->serve();