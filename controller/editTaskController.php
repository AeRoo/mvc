<?php

require_once "./controller/abstractController.php";

class EditTaskController extends AbstractController {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function handle() {
        if (!isset($_GET["id"])) {
            echo "missing task id";
            die();
        }
        $this->model->setTaskId($_GET["id"]);
        if (isset($_POST["title"]) && isset($_POST["content"])) {
            $this->model->updateTask($_POST["title"], $_POST["content"]);
            $this->redirect("/");
        }
        if (!$this->model->update()) {
            $this->model->showError("No such task");
        }
    }
}