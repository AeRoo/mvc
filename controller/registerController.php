<?php

require_once "./controller/abstractController.php";

class RegisterController extends AbstractController {
    private $model;
    private $login_pattern = "/[0-9a-zA-Z]{4,}/i";
    private $password_pattern = "/^[a-zA-Z0-9]{7,}$/i";

    public function __construct($model) {
        $this->model = $model;
    }

    public function handle() {
        if (isset($_SESSION["auth"]) && $_SESSION["auth"] == true) {
            $this->redirect("/");
        }
        if (!isset($_POST["login"]) || !isset($_POST["password"])) {
            return;
        }
        if (preg_match($this->login_pattern, $_POST["login"]) == 0) {
            $this->model->showError("You cannot use this username");
            return;
        }
        if (preg_match($this->password_pattern, $_POST["password"]) == 0) {
            $this->model->showError("You cannot use this password");
            return;
        }
        if (!$this->model->register($_POST["login"], $_POST["password"])) {
            $this->model->showError("This user already exists");
            return;
        }
        $this->redirect("/");
    }
}