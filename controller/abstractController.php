<?php

class AbstractController {
    function redirect($url) {
        header("Location: " . $url, true, 301);
        die();
    }

    public function handle() {

    }
}