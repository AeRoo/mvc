<?php

require_once "./controller/abstractController.php";

class AddTaskController extends AbstractController {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function handle() {
        if (!isset($_POST["title"]) || !isset($_POST["content"])) {
            echo "missing required key";
            die();
        }

        $this->model->addTask($_POST["title"], $_POST["content"]);
        $this->redirect("/");
    }
}