<?php

require_once "./controller/abstractController.php";

class LogoutController extends AbstractController {
    public function __construct($model) {
    }

    public function handle() {
        session_destroy();
        $this->redirect("/login");
    }
}