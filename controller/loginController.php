<?php

require_once "./controller/abstractController.php";

class LoginController extends AbstractController {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function handle() {
        if (isset($_SESSION["auth"]) && $_SESSION["auth"] == true) {
            $this->redirect("/");
        }
        if (isset($_POST["login"]) && isset($_POST["password"])) {
            if (!$this->model->login($_POST["login"], $_POST["password"])) {
                $this->model->showError("Invalid login or password");
                return;
            }
            $this->redirect("/");
        }
    }
}