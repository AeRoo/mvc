<?php

require_once "./controller/abstractController.php";

class DeleteTaskController extends AbstractController {
    private $model;

    public function __construct($model) {
        $this->model = $model;
    }

    public function handle() {
        if (!isset($_GET["id"])) {
            echo "missing task id";
            die();
        }
        $this->model->setTaskId($_GET["id"]);
        $this->model->deleteTask();
        $this->redirect("/");
    }
}